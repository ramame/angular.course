import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})


export class RegisterComponent implements OnInit {

  email: string;
  password: string;
  name: string;
  error;
  
  
  signup(){
     this.authService.signup(this.email,this.password)
    .then(value =>{
      this.authService.updateProfile(value.user,this.name);
      this.authService.addUser(value.user, this.name);
    }).then(value1 =>{ 
      this.router.navigate(['/todos']);
    }).catch(err => {
       this.error = err;
    
       console.log(err);
      })
  
                                                  //זה לא אותו וליו מלפני


 
  }


 
  constructor(private authService:AuthService, private router:Router) { }   //האובייקט יהיה באותיות קטנות כמו שם המחלקה

  ngOnInit() {
  }

}

