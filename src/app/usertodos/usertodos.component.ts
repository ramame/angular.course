import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
@Component({
  selector: 'app-usertodos',
  templateUrl: './usertodos.component.html',
  styleUrls: ['./usertodos.component.css']
})
export class UsertodosComponent implements OnInit {

user = 'jack'
 
todos = [

]



  constructor(private db:AngularFireDatabase) { }

  changeuser(){
    this.db.list('/users/'+this.user+'/todos').snapshotChanges().subscribe(
      todos =>{                   //this.todos זה זה מלמעלה
                                    //סתם todos יהיה זה מתוך הדטה בייס
      this.todos = [];   //נאפס את כל הטודוס
      todos.forEach(                   
      todo => {                       // לכל אחד מהמשתנים במערך נקרא באופן זמני טודו
      let y = todo.payload.toJSON();        //y is a  temp variable,  we fill him (y) with payload us json.
                                         //payload came us string so we need change it to json
      y["$key"] = todo.key;             //אני מוסיף לכל טודו עוד תכונה שיקראו לה דולר קיי ובתוכה יהיה שם המפתח
                                         //key מילה שמורה
      this.todos.push(y);              //מכניס את האיבר שיצרתי בתוך טודוס שלנו
  }
    ) 
  }
  
  
  
      )

  }
  
  ngOnInit() {
    this.db.list('/users/'+this.user+'/todos').snapshotChanges().subscribe(
    todos =>{                   //this.todos זה זה מלמעלה
                                  //סתם todos יהיה זה מתוך הדטה בייס
    this.todos = [];   //נאפס את כל הטודוס
    todos.forEach(                   
    todo => {                       // לכל אחד מהמשתנים במערך נקרא באופן זמני טודו
    let y = todo.payload.toJSON();        //y is a  temp variable,  we fill him (y) with payload us json.
                                       //payload came us string so we need change it to json
    y["$key"] = todo.key;             //אני מוסיף לכל טודו עוד תכונה שיקראו לה דולר קיי ובתוכה יהיה שם המפתח
                                       //key מילה שמורה
    this.todos.push(y);              //מכניס את האיבר שיצרתי בתוך טודוס שלנו
}
  ) 
}



    )
  }

}
