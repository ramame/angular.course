import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import { Router } from "@angular/router";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

email:string;
password:string;
error = '';

Login(){
 this.Auth.login(this.email,this.password).then(value => {console.log(value)})
 .then(value => {this.router.navigate(['/todos'])})
 .catch(err => {this.error = err;})
}

  constructor(private Auth:AuthService, private router:Router) { }

  ngOnInit() {
  }

}
