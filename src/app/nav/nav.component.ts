import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import {AuthService} from '../auth.service';
//import { RouterInitializer } from '@angular/router/src/router_module';

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {


   logout(){
  this.Auth.logout().then(value => {
    this.router.navigate(['/login']);
  }).catch(err => {console.log(err)})
  }

  toRegister(){
  this.router.navigate(['/register']);
  }

  toCodes(){
    this.router.navigate(['/codes']);
  }

   toUserTodos(){
      this.router.navigate(['/usertodos']);
    }

  constructor(private router:Router, private Auth:AuthService) { }

  ngOnInit() {
  }

}
