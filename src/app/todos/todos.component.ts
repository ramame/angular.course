import { Component, OnInit } from '@angular/core';
import { Subscriber } from 'rxjs';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {AuthService} from '../auth.service';
import {TodosService} from '../todos.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  text:string;
  todos = [

  ]

  addTodo(){
    this.todosS.addTodo(this.text);
    this.text = '';
  }  

   text1;
   showText($event){
   this.text1= $event;

  }

  constructor(private db:AngularFireDatabase, private Auth:AuthService, private todosS:TodosService) { }
                                            
  ngOnInit() {
    this.Auth.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/todos').snapshotChanges().subscribe(
        todos =>{                   //this.todos זה זה מלמעלה
                                    //סתם todos יהיה זה מתוך הדטה בייס
        this.todos = [];            //נאפס את כל הטודוס
        todos.forEach(                   
        todo => {                          // לכל אחד מהמשתנים במערך נקרא באופן זמני טודו
        let y = todo.payload.toJSON();     //y is a  temp variable,  we fill him (y) with payload us json.
                                             //payload came us string so we need change it to json
        y["$key"] = todo.key;              //אני מוסיף לכל טודו עוד תכונה שיקראו לה דולר קיי ובתוכה יהיה שם המפתח
                                           //key מילה שמורה
        this.todos.push(y);                //מכניס את האיבר שיצרתי בתוך טודוס שלנו
    }
      ) 
    }
    
    
    
        )  
    })
  
  }

}
