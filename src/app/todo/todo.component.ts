import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TodosService } from '../todos.service';

@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})

export class TodoComponent implements OnInit {
  @Input() data:any;
  @Output() myButtonClicked = new EventEmitter<any>();
text;
id;
key:string;
Showeditfield;
showButton =false;
savecancel:string

showEdit(){
  this.savecancel = this.text
  this.Showeditfield = true;
  
}

cancel(){
  this.text=this.savecancel
  this.Showeditfield = false;
 
}
  
save(){
  
  this.todosS.update(this.key,this.text)
  this.Showeditfield = false;
}
   

ToShow(){
this.showButton = true;

}

ToHide (){
  this.showButton = false;
}


toSend () {
console.log('event cought');
this.myButtonClicked.emit(this.text);
console.log(this.key);
this.todosS.deleteTodo(this.key);
}

  constructor(private todosS:TodosService) { }

  ngOnInit() {
  this.text = this.data.text;
  this.id = this.data.id;
  this.key = this.data.$key;

  }

}
