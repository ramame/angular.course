import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class TodosService {
 
  deleteTodo(key){
    this.auth.user.subscribe(user =>{
    this.db.list('/users/'+user.uid+'/todos/').remove(key);})
  }

  update(key,text){
   this.auth.user.subscribe(user => {
   this.db.list('/users/'+user.uid+'/todos/').update(key, {'text':text})
  //אני מעביר ל-אפדייט את הקיי ומתוך הקיי הזה איזה שדה לעדכן לשים לב לצורת הכתיבה

   })
  }
 
addTodo(text){
  this.auth.user.subscribe(user => {
  let ref = this.db.database.ref('/');
  ref.child('users').child(user.uid).child('todos').push({'text':text}); 
 })
}


  constructor(private auth:AuthService, private db:AngularFireDatabase) { }
}
